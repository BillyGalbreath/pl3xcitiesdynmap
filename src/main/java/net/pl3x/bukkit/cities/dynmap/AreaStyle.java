package net.pl3x.bukkit.cities.dynmap;

import org.bukkit.configuration.file.FileConfiguration;
import org.dynmap.markers.MarkerIcon;

public class AreaStyle {
    String strokecolor;
    double strokeopacity;
    int strokeweight;
    String fillcolor;
    double fillopacity;
    String homemarker;
    MarkerIcon homeicon;
    boolean boost;

    AreaStyle(FileConfiguration cfg, String path, AreaStyle def) {
        strokecolor = cfg.getString(path + ".strokeColor", def.strokecolor);
        strokeopacity = cfg.getDouble(path + ".strokeOpacity", def.strokeopacity);
        strokeweight = cfg.getInt(path + ".strokeWeight", def.strokeweight);
        fillcolor = cfg.getString(path + ".fillColor", def.fillcolor);
        fillopacity = cfg.getDouble(path + ".fillOpacity", def.fillopacity);
        homemarker = cfg.getString(path + ".homeicon", def.homemarker);
        if (homemarker != null) {
            homeicon = Main.markerapi.getMarkerIcon(homemarker);
            if (homeicon == null) {
                Logger.error("Invalid homeicon: " + homemarker);
                homeicon = Main.markerapi.getMarkerIcon("blueicon");
            }
        }
        boost = cfg.getBoolean(path + ".boost", def.boost);
    }

    AreaStyle(FileConfiguration cfg, String path) {
        strokecolor = cfg.getString(path + ".strokeColor", "#FF0000");
        strokeopacity = cfg.getDouble(path + ".strokeOpacity", 0.8);
        strokeweight = cfg.getInt(path + ".strokeWeight", 3);
        fillcolor = cfg.getString(path + ".fillColor", "#FF0000");
        fillopacity = cfg.getDouble(path + ".fillOpacity", 0.35);
        homemarker = cfg.getString(path + ".homeicon", null);
        if (homemarker != null) {
            homeicon = Main.markerapi.getMarkerIcon(homemarker);
            if (homeicon == null) {
                Logger.error("Invalid homeicon: " + homemarker);
                homeicon = Main.markerapi.getMarkerIcon("blueicon");
            }
        }
        boost = cfg.getBoolean(path + ".boost", false);
    }
}
