package net.pl3x.bukkit.cities.dynmap;

import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.protection.City;
import net.pl3x.bukkit.cities.protection.CityChunk;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.dynmap.DynmapAPI;
import org.dynmap.markers.AreaMarker;
import org.dynmap.markers.Marker;
import org.dynmap.markers.MarkerAPI;
import org.dynmap.markers.MarkerIcon;
import org.dynmap.markers.MarkerSet;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class Main extends JavaPlugin {
    private static final String DEF_INFOWINDOW = "<div class=\"infowindow\"><span style=\"font-size:120%;\">%name%</span><br /> Owners <span style=\"font-weight:bold;\">%owners%</span><br /> Population <span style=\"font-weight:bold;\">%population%</span></div>";

    protected Plugin dynmap;
    protected Plugin pl3xcities;
    protected static MarkerAPI markerapi;
    protected DynmapAPI api;

    private int blocksize;
    private MarkerSet set;
    private String infowindow;
    private AreaStyle defstyle;
    private Map<String, AreaStyle> cusstyle;
    private boolean stop;

    private Map<String, AreaMarker> resareas = new HashMap<>();
    private Map<String, Marker> resmark = new HashMap<>();

    private String formatInfoWindow(City city) {
        String v = "<div class=\"regioninfo\">" + infowindow + "</div>";
        v = v.replace("%name%", ChatColor.stripColor(city.getName()));
        Map<UUID, String> owners = city.getOwners();
        String ownersString = "";
        if (owners != null && !owners.isEmpty()) {
            for (String owner : owners.values()) {
                if (!ownersString.equals("")) {
                    ownersString += ", ";
                }
                ownersString += owner;
            }
        }
        v = v.replace("%owners%", ownersString);
        v = v.replace("%population%", Integer.toString(city.getPopulation()));
        /* Build flags */
        // String flgs = "open: " + fact.isOpen();
        // for (FFlag ff : FFlag.values()) {
        // flgs += "<br/>" + ff.getNicename() + ": " + fact.getFlag(ff);
        // v = v.replace("%flag." + ff.name() + "%", fact.getFlag(ff) ? "true" : "false");
        // }
        // v = v.replace("%flags%", flgs);
        return v;
    }

    private void addStyle(String resid, AreaMarker m) {
        AreaStyle as = cusstyle.get(resid);
        if (as == null) {
            as = defstyle;
        }
        int sc = 0xFF0000;
        int fc = 0xFF0000;
        try {
            sc = Integer.parseInt(as.strokecolor.substring(1), 16);
            fc = Integer.parseInt(as.fillcolor.substring(1), 16);
        } catch (NumberFormatException nfx) {
            //ignore
        }
        m.setLineStyle(as.strokeweight, as.strokeopacity, sc);
        m.setFillStyle(as.fillopacity, fc);
        m.setBoostFlag(as.boost);
    }

    private MarkerIcon getMarkerIcon(City city) {
        AreaStyle as = cusstyle.get(city.getName());
        if (as == null) {
            as = defstyle;
        }
        return as.homeicon;
    }

    enum direction {
        XPLUS,
        ZPLUS,
        XMINUS,
        ZMINUS
    }

    private int floodFillTarget(TileFlags src, TileFlags dest, int x, int y) {
        int cnt = 0;
        ArrayDeque<int[]> stack = new ArrayDeque<>();
        stack.push(new int[]{
                x, y
        });

        while (!stack.isEmpty()) {
            int[] nxt = stack.pop();
            x = nxt[0];
            y = nxt[1];
            if (src.getFlag(x, y)) { /* Set in src */
                src.setFlag(x, y, false); /* Clear source */
                dest.setFlag(x, y, true); /* Set in destination */
                cnt++;
                if (src.getFlag(x + 1, y))
                    stack.push(new int[]{
                            x + 1, y
                    });
                if (src.getFlag(x - 1, y))
                    stack.push(new int[]{
                            x - 1, y
                    });
                if (src.getFlag(x, y + 1))
                    stack.push(new int[]{
                            x, y + 1
                    });
                if (src.getFlag(x, y - 1))
                    stack.push(new int[]{
                            x, y - 1
                    });
            }
        }
        return cnt;
    }

    private static class CityBlock {
        int x;
        int z;
    }

    private static class CityBlocks {
        Map<String, LinkedList<CityBlock>> blocks = new HashMap<>();
    }

    /* Handle specific faction on specific world */
    private void handleCityOnWorld(City city, LinkedList<CityBlock> blocks, Map<String, AreaMarker> newmap) {
        double[] x, z;
        int poly_index = 0; /* Index of polygon for given city */

		/* Build popup */
        String desc = formatInfoWindow(city);

		/* Handle areas */
        if (blocks.isEmpty()) {
            return;
        }
        LinkedList<CityBlock> nodevals = new LinkedList<>();
        TileFlags curblks = new TileFlags();
        /* Loop through blocks: set flags on blockmaps */
        for (CityBlock b : blocks) {
            curblks.setFlag(b.x, b.z, true); /* Set flag for block */
            nodevals.addLast(b);
        }
        /* Loop through until we don't find more areas */
        while (nodevals != null) {
            LinkedList<CityBlock> ournodes = null;
            LinkedList<CityBlock> newlist = null;
            TileFlags ourblks = null;
            int minx = Integer.MAX_VALUE;
            int minz = Integer.MAX_VALUE;
            for (CityBlock node : nodevals) {
                int nodex = node.x;
                int nodez = node.z;
                /* If we need to start shape, and this block is not part of one yet */
                if ((ourblks == null) && curblks.getFlag(nodex, nodez)) {
                    ourblks = new TileFlags(); /* Create map for shape */
                    ournodes = new LinkedList<>();
                    floodFillTarget(curblks, ourblks, nodex, nodez); /* Copy shape */
                    ournodes.add(node); /* Add it to our node list */
                    minx = nodex;
                    minz = nodez;
                }
                /* If shape found, and we're in it, add to our node list */
                else if ((ourblks != null) && ourblks.getFlag(nodex, nodez)) {
                    ournodes.add(node);
                    if (nodex < minx) {
                        minx = nodex;
                        minz = nodez;
                    } else if ((nodex == minx) && (nodez < minz)) {
                        minz = nodez;
                    }
                } else { /* Else, keep it in the list for the next polygon */
                    if (newlist == null)
                        newlist = new LinkedList<>();
                    newlist.add(node);
                }
            }
            nodevals = newlist; /* Replace list (null if no more to process) */
            if (ourblks != null) {
                /* Trace outline of blocks - start from minx, minz going to x+ */
                int cur_x = minx;
                int cur_z = minz;
                direction dir = direction.XPLUS;
                ArrayList<int[]> linelist = new ArrayList<>();
                linelist.add(new int[]{
                        minx, minz
                }); // Add start point
                while ((cur_x != minx) || (cur_z != minz) || (dir != direction.ZMINUS)) {
                    switch (dir) {
                        case XPLUS: /* Segment in X+ direction */
                            if (!ourblks.getFlag(cur_x + 1, cur_z)) { /* Right turn? */
                                linelist.add(new int[]{
                                        cur_x + 1, cur_z
                                }); /* Finish line */
                                dir = direction.ZPLUS; /* Change direction */
                            } else if (!ourblks.getFlag(cur_x + 1, cur_z - 1)) { /* Straight? */
                                cur_x++;
                            } else { /* Left turn */
                                linelist.add(new int[]{
                                        cur_x + 1, cur_z
                                }); /* Finish line */
                                dir = direction.ZMINUS;
                                cur_x++;
                                cur_z--;
                            }
                            break;
                        case ZPLUS: /* Segment in Z+ direction */
                            if (!ourblks.getFlag(cur_x, cur_z + 1)) { /* Right turn? */
                                linelist.add(new int[]{
                                        cur_x + 1, cur_z + 1
                                }); /* Finish line */
                                dir = direction.XMINUS; /* Change direction */
                            } else if (!ourblks.getFlag(cur_x + 1, cur_z + 1)) { /* Straight? */
                                cur_z++;
                            } else { /* Left turn */
                                linelist.add(new int[]{
                                        cur_x + 1, cur_z + 1
                                }); /* Finish line */
                                dir = direction.XPLUS;
                                cur_x++;
                                cur_z++;
                            }
                            break;
                        case XMINUS: /* Segment in X- direction */
                            if (!ourblks.getFlag(cur_x - 1, cur_z)) { /* Right turn? */
                                linelist.add(new int[]{
                                        cur_x, cur_z + 1
                                }); /* Finish line */
                                dir = direction.ZMINUS; /* Change direction */
                            } else if (!ourblks.getFlag(cur_x - 1, cur_z + 1)) { /* Straight? */
                                cur_x--;
                            } else { /* Left turn */
                                linelist.add(new int[]{
                                        cur_x, cur_z + 1
                                }); /* Finish line */
                                dir = direction.ZPLUS;
                                cur_x--;
                                cur_z++;
                            }
                            break;
                        case ZMINUS: /* Segment in Z- direction */
                            if (!ourblks.getFlag(cur_x, cur_z - 1)) { /* Right turn? */
                                linelist.add(new int[]{
                                        cur_x, cur_z
                                }); /* Finish line */
                                dir = direction.XPLUS; /* Change direction */
                            } else if (!ourblks.getFlag(cur_x - 1, cur_z - 1)) { /* Straight? */
                                cur_z--;
                            } else { /* Left turn */
                                linelist.add(new int[]{
                                        cur_x, cur_z
                                }); /* Finish line */
                                dir = direction.XMINUS;
                                cur_x--;
                                cur_z--;
                            }
                            break;
                    }
                }
                /* Build information for specific area */
                String polyid = city.getName() + "__" + poly_index;
                int sz = linelist.size();
                x = new double[sz];
                z = new double[sz];
                for (int i = 0; i < sz; i++) {
                    int[] line = linelist.get(i);
                    x[i] = (double) line[0] * (double) blocksize;
                    z[i] = (double) line[1] * (double) blocksize;
                }
                /* Find existing one */
                AreaMarker m = resareas.remove(polyid); /* Existing area? */
                if (m == null) {
                    m = set.createAreaMarker(polyid, city.getName(), false, city.getSpawn().getWorld().getName(), x, z, false);
                    if (m == null) {
                        Logger.info("error adding area marker " + polyid);
                        return;
                    }
                } else {
                    m.setCornerLocations(x, z); /* Replace corner locations */
                    m.setLabel(city.getName()); /* Update label */
                }
                m.setDescription(desc); /* Set popup */

				/* Set line and fill properties */
                addStyle(city.getName(), m);

				/* Add to map */
                newmap.put(polyid, m);
                poly_index++;
            }
        }

    }

    /* Update Cities information */
    protected void updateCities() {
        Map<String, AreaMarker> newmap = new HashMap<>(); /* Build new map */
        Map<String, Marker> newmark = new HashMap<>(); /* Build new map */

		/* Parse into city centric mapping, split by world */
        Map<String, CityBlocks> blocks_by_city = new HashMap<>();

        for (City city : CityManager.getManager().getCities()) {
            Set<CityChunk> chunks = city.getChunks();
            CityBlocks cityblocks = blocks_by_city.get(city.getName()); /* Look up faction */
            if (cityblocks == null) { /* Create faction block if first time */
                cityblocks = new CityBlocks();
                blocks_by_city.put(city.getName(), cityblocks);
            }

            for (CityChunk chunk : chunks) {
                String world = chunk.getWorld().getName();

				/* Get block set for given world */
                LinkedList<CityBlock> blocks = cityblocks.blocks.get(world);
                if (blocks == null) {
                    blocks = new LinkedList<>();
                    cityblocks.blocks.put(world, blocks);
                }
                CityBlock block = new CityBlock();
                block.x = chunk.getX();
                block.z = chunk.getZ();
                blocks.add(block); /* Add to list */
            }
        }
        /* Loop through factions */
        for (City city : CityManager.getManager().getCities()) {
            CityBlocks cityblocks = blocks_by_city.get(city.getName()); /* Look up city */
            if (cityblocks == null)
                continue;

			/* Loop through each world that faction has blocks on */
            for (Map.Entry<String, LinkedList<CityBlock>> worldblocks : cityblocks.blocks.entrySet()) {
                handleCityOnWorld(city, worldblocks.getValue(), newmap);
            }
            cityblocks.blocks.clear();

			/* Now, add marker for home location */
            Location homeloc = city.getSpawn();
            if (homeloc != null) {
                String markid = city.getName() + "__home";
                MarkerIcon ico = getMarkerIcon(city);
                if (ico != null) {
                    Marker home = resmark.remove(markid);
                    String lbl = city.getName() + " [spawn]";
                    if (home == null) {
                        home = set.createMarker(markid, lbl, homeloc.getWorld().getName(), homeloc.getBlockX(), homeloc.getBlockY(), homeloc.getBlockZ(), ico, false);
                    } else {
                        home.setLocation(homeloc.getWorld().getName(), homeloc.getBlockX(), homeloc.getBlockY(), homeloc.getBlockZ());
                        home.setLabel(lbl); /* Update label */
                        home.setMarkerIcon(ico);
                    }
                    if (home != null) {
                        home.setDescription(formatInfoWindow(city)); /* Set popup */
                        newmark.put(markid, home);
                    }
                }
            }
        }
        blocks_by_city.clear();

		/* Now, review old map - anything left is gone */
        resareas.values().forEach(AreaMarker::deleteMarker);
        resmark.values().forEach(Marker::deleteMarker);
		/* And replace with new map */
        resareas = newmap;
        resmark = newmark;

    }

    public void onEnable() {
        saveDefaultConfig();
        Logger.info("initializing");
        PluginManager pm = Bukkit.getServer().getPluginManager();
		/* Get dynmap */
        dynmap = pm.getPlugin("dynmap");
        if (dynmap == null) {
            Logger.error("Cannot find dynmap!");
            return;
        }
        api = (DynmapAPI) dynmap; /* Get API */
		/* Get Factions */
        Plugin p = pm.getPlugin("Pl3xCities");
        if (p == null) {
            Logger.error("Cannot find Pl3xCities!");
            return;
        }
        pl3xcities = p;

		/* If both enabled, activate */
        if (dynmap.isEnabled() && pl3xcities.isEnabled()) {
            activate();
        }

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    private boolean reload = false;

    protected void activate() {
        markerapi = api.getMarkerAPI();
        if (markerapi == null) {
            Logger.error("Error loading dynmap marker API!");
            return;
        }

        blocksize = 16; /* Fixed at 16 */

		/* Load configuration */
        if (reload) {
            this.reloadConfig();
            if (set != null) {
                set.deleteMarkerSet();
                set = null;
            }
        } else {
            reload = true;
        }
        FileConfiguration cfg = getConfig();

		/* Now, add marker set for cities (make it transient) */
        set = markerapi.getMarkerSet("pl3xcities.markerset");
        if (set == null) {
            set = markerapi.createMarkerSet("pl3xcities.markerset", cfg.getString("layer.name", "Cities"), null, false);
        } else {
            set.setMarkerSetLabel(cfg.getString("layer.name", "Cities"));
        }
        if (set == null) {
            Logger.error("Error creating marker set");
            return;
        }
		/* Make sure these are empty (on reload) */
        resareas.clear();
        resmark.clear();

        int minzoom = cfg.getInt("layer.minzoom", 0);
        if (minzoom > 0)
            set.setMinZoom(minzoom);
        set.setLayerPriority(cfg.getInt("layer.layerprio", 10));
        infowindow = cfg.getString("infowindow", DEF_INFOWINDOW);

		/* Get style information */
        defstyle = new AreaStyle(cfg, "regionstyle");
        cusstyle = new HashMap<>();
        ConfigurationSection sect = cfg.getConfigurationSection("custstyle");
        if (sect != null) {
            Set<String> ids = sect.getKeys(false);

            for (String id : ids) {
                cusstyle.put(id, new AreaStyle(cfg, "custstyle." + id, defstyle));
            }
        }

		/* Set up update job - based on period */
        int per = cfg.getInt("update-period", 300);
        if (per < 15) {
            per = 15;
        }
        long updperiod = (per * 20);
        stop = false;

        new CitiesUpdate().runTaskTimer(this, 40, updperiod); /* First time is 2 seconds */
        // getServer().getPluginManager().registerEvents(new BukkitListener(this), this);

        Logger.info("version " + this.getDescription().getVersion() + " is activated");
    }

    public void onDisable() {
        if (set != null) {
            set.deleteMarkerSet();
            set = null;
        }
        resareas.clear();
        stop = true;
    }

    private class CitiesUpdate extends BukkitRunnable {
        public void run() {
            if (!stop) {
                updateCities();
            } else {
                cancel();
            }
        }
    }
}
